import joi from "joi"

export const PostKursValidator = joi.object({
    symbol: joi.string().required(),
    e_rate: joi.object({
        jual: joi.number(),
        beli: joi.number()
    }),
    tt_counter: joi.object({
        jual: joi.number(),
        beli: joi.number()
    }),
    bank_notes: joi.object({
        jual: joi.number(),
        beli: joi.number()
    }),
    date: joi.date().required()
})