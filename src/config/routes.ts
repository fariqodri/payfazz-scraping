import { Router } from "express"

import {IndexingController, KursController} from "../controllers"

const routes = Router()

routes.get("/indexing/", IndexingController.get!)

routes.delete("/kurs/:date", KursController.delete!)
routes.get("/kurs/:symbol", KursController.getWithCurrency)
routes.get("/kurs/", KursController.get!)
routes.post("/kurs/", KursController.create!)
routes.put("/kurs/", KursController.update!)

export default routes