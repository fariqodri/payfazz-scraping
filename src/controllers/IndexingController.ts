import rp from 'request-promise';
import $ from 'cheerio';
import moment from "moment"

import { IController, IndexedDataModel, KursModel } from '../models';
import chunk from "../libs/chunk"

interface ICurrencyKurs {
    [key: string]: number[][]
}

interface IHead {
    type: string,
    date: Date
}

const BASE_URL =
    'https://www.bca.co.id/id/Individu/Sarana/Kurs-dan-Suku-Bunga/Kurs-dan-Kalkulator';

export const IndexingController: IController = {
    async get(_, res) {
        const head = await getTypeAndTimeHeader(BASE_URL)
        const currencyKurs = await getKursValue(BASE_URL)(head.length)
        await saveToDb(currencyKurs, head)
        res.json({
            kurs: currencyKurs,
            head
        })
    }
};

const getKursValue = (url: string) => async (totalTypes: number) => {
    const html = await rp(url);
    const tableRows = $('div.kurs-e-rate table tbody tr', html);

    const kursValues = tableRows.toArray().map(v => $(v).find("td").toArray().map(v => $(v).text()))
    const availableCurrencies = kursValues.reduce((prev, curr) => {
        const currency = curr[0]
        const values = curr.slice(1).map(v => parseFloat(v.replace(/\./g, "").replace(/,/g, ".")))
        prev[currency as any] = chunk(values, Math.floor(values.length / totalTypes))
        return prev
    }, {} as ICurrencyKurs)
    return availableCurrencies
};

const getTypeAndTimeHeader = async (url: string) => {
    const html = await rp(url);
    const upperHeader = $('div.kurs-e-rate table thead tr', html).get(0);

    const typeAndTimeSeparator = $("th", upperHeader).find("br")

    const typeAndTime: IHead[] = typeAndTimeSeparator.toArray().map((v, _) => ({
        type: v.prev.data!.replace(/\*/, "").replace(/ |-/g, "_").toLowerCase(),
        date: parseDateStringToDate(v.next.data)
    }))

    return typeAndTime
};

const parseDateStringToDate = (dateString?: string) => {
    const cleanDateString = dateString!.replace(/( \/ )|(WIB)/g, " ").trim()
    return moment(cleanDateString).toDate()
}

const saveToDb = async (kursValues: ICurrencyKurs, head: IHead[]) => {
    head.forEach((v, i) => {
        IndexedDataModel.findOne({type: v.type, datetime: v.date}, (err, indexedData) => {
            if (err) {
                console.error(err)
            } else {
                if (indexedData == null) {
                    let kursValuesForSuchTypes = []
                    for (let key in kursValues) {
                        const kursInstance = createKursJualAtauBeliInstance(kursValues, i)
                        const kursJualDb = kursInstance(key, "jual")
                        const kursBeliDb = kursInstance(key, "beli")
                        kursValuesForSuchTypes.push(kursBeliDb); kursValuesForSuchTypes.push(kursJualDb)
                    }
                    const indexedDataNew = new IndexedDataModel({
                        type: v.type,
                        datetime: v.date,
                        kurs_data: kursValuesForSuchTypes
                    })
                    indexedDataNew.save()
                }
            }
        })
    })
}

const createKursJualAtauBeliInstance = (kursValues: ICurrencyKurs, type_index: number) => (currency: string, kurs_type: "jual"|"beli") => {
    // Jual = 0, Beli = 1
    return new KursModel({
        currency,
        price: kurs_type === "jual" ? kursValues[currency][type_index][0] : kursValues[currency][type_index][1],
        kurs_type
    })
}