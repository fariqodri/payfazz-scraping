import moment, { Moment } from 'moment';
import joi from 'joi';

import {
    IController,
    IIndexedData,
    KursModel,
    IndexedDataModel
} from '../models';
import { Handler } from 'express';
import mongoose from 'src/config/database';
import { PostKursValidator } from '../validators/PostKursValidator';

interface IKursController extends IController {
    getWithCurrency: Handler;
}

type FindCallback<T> = (err: any, result: T[]) => void;

export const KursController: IKursController = {
    async delete(req, res) {
        const date = getStartOfDay(getDate(req.params.date));
        const result = IndexedDataModel.deleteMany(
            {
                datetime:
                    date != null
                        ? {
                              $gte: date.toDate(),
                              $lte: date.endOf('day').toDate()
                          }
                        : {}
            },
            err => {
                if (err) {
                    return res.status(400).json({ message: err.message });
                } else {
                    return res.status(200).json({ message: 'success' });
                }
            }
        );
    },

    async getWithCurrency(req, res) {
        const currency: string = req.params.symbol;
        let startDate: Moment | null | Date = getStartOfDay(
            getDate(req.query.startdate)
        );
        let endDate: Moment | null | Date = getEndOfDay(
            getDate(req.query.enddate)
        );
        if (startDate) {
            startDate = startDate.toDate();
        }
        if (endDate) {
            endDate = endDate.toDate();
        }
        IndexedDataModel.aggregate([
            {
                $match: {
                    'kurs_data.currency': currency,
                    datetime: {
                        $gte: startDate,
                        $lte: endDate
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 1,
                    datetime: 1,
                    kurs_data: {
                        $filter: {
                            input: '$kurs_data',
                            as: 'kurs',
                            cond: { $eq: ['$$kurs.currency', currency] }
                        }
                    }
                }
            }
        ])
            .then(result => res.json({ data: result }))
            .catch(err => res.status(400).send(err.message));
    },

    async get(req, res) {
        let startDate: Moment | null | Date = getStartOfDay(
            getDate(req.query.startdate)
        );
        let endDate: Moment | null | Date = getEndOfDay(
            getDate(req.query.enddate)
        );
        if (startDate) {
            startDate = startDate.toDate();
        }
        if (endDate) {
            endDate = endDate.toDate();
        }
        IndexedDataModel.find(
            { datetime: { $gte: startDate, $lte: endDate } },
            (err, result) => {
                if (err) {
                    return res.status(400);
                }
                return res.json({
                    data: result.map(v => {
                        const { kurs_data, type, datetime } = v;
                        return {
                            type,
                            datetime,
                            kurs_data
                        };
                    })
                });
            }
        );
    },

    async create(req, res) {
        joi.validate(req.body, PostKursValidator, async (err, value) => {
            if (err) {
                return res.status(400).send(err.message)
            }
            const { symbol, date, ...types } = value;
            const typesArr = [];
            for (let type in types) {
                typesArr.push(type);
            }
            searchIndexedDataBy(IndexedDataModel)(date, typesArr, symbol)(
                (err, result) => {
                    if (err) {
                        return res.status(400).send(err.message);
                    } else {
                        if (result.length == 0) {
                            const kursData = {
                                currency: symbol
                            };
                            const resultDatas = [];
                            try {
                                for (let type in types) {
                                    const indexedData = new IndexedDataModel({
                                        type,
                                        datetime: moment(date).toDate(),
                                        kurs_data: []
                                    });
                                    const kursJual = new KursModel(kursData);
                                    const kursBeli = new KursModel(kursData);
                                    kursJual.kurs_type = 'jual';
                                    kursJual.price = types[type]['jual'];
                                    kursBeli.kurs_type = 'beli';
                                    kursBeli.price = types[type]['beli'];
                                    indexedData.kurs_data.push(kursJual);
                                    indexedData.kurs_data.push(kursBeli);
                                    indexedData.save();
                                    resultDatas.push(indexedData);
                                }
                            } catch (err) {
                                return res.status(400).send(err.message);
                            }
                            return res.json({ data: resultDatas });
                        } else {
                            return res
                                .status(400)
                                .json({ message: 'already exists' });
                        }
                    }
                }
            );
        });
    },

    async update(req, res) {
        joi.validate(req.body, PostKursValidator, async (err, value) => {
            if (err) {
                return res.status(400).send(err.message)
            }

            const { symbol, date, ...types } = value;
            const momentDate = getDate(date);
            
            const condition = {
                'kurs_data.currency': symbol,
                datetime: momentDate
                    ? {
                          $gte: momentDate.toDate(),
                          $lte: moment(momentDate)
                              .endOf('day')
                              .toDate()
                      }
                    : {}
            };
    
            const result: any[] = [];
    
            for (let type in types) {
                const kursJual = types[type]['jual'];
                const kursBeli = types[type]['beli'];
                let kursUpdated = await IndexedDataModel.findOne({
                    type: type,
                    ...condition
                });
                if (kursUpdated) {
                    try {
                        if (kursJual) {
                            await IndexedDataModel.updateOne(
                                { _id: kursUpdated._id },
                                { $set: { 'kurs_data.$[kurs].price': kursJual } },
                                { arrayFilters: [{ 'kurs.kurs_type': 'jual' }] }
                            );
                        }
                        if (kursBeli) {
                            await IndexedDataModel.updateOne(
                                { _id: kursUpdated._id },
                                { $set: { 'kurs_data.$[kurs].price': kursBeli } },
                                { arrayFilters: [{ 'kurs.kurs_type': 'beli' }] }
                            );
                        }
                        kursUpdated = await IndexedDataModel.findById(
                            kursUpdated._id
                        );
                        const { kurs_data, type, datetime } = kursUpdated!;
                        result.push({
                            type,
                            datetime,
                            kurs_data: kurs_data.filter(v => v.currency === symbol)
                        });
                    } catch (err) {
                        console.error();
                        return res.status(404).send('Not found');
                    }
                }
            }
            return res.json({ data: result });
        })
    }
};

const getDate = (date: string) => {
    return moment(date).isValid() ? moment(date) : null;
};

const searchModelBy = <T extends mongoose.Document>(
    model: mongoose.Model<T>
) => (conditions: any) => (callback: FindCallback<T>) => {
    model.find(conditions, callback);
};

const searchIndexedDataBy = (model: mongoose.Model<IIndexedData>) => (
    datetime: string,
    type: string[],
    currency: string | null = null
) => {
    const momentDate = getDate(datetime);
    return searchModelBy(model)({
        datetime: momentDate
            ? {
                  $gte: momentDate,
                  $lte: moment(momentDate)
                      .endOf('day')
                      .toDate()
              }
            : {},
        type: { $in: type },
        'kurs_data.currency': currency
    });
};

const getStartOfDay = (time: Moment | null) => {
    if (time) {
        return time.startOf('day');
    }
    return null;
};

const getEndOfDay = (time: Moment | null) => {
    if (time) {
        return time.endOf('day');
    }
    return null;
};
