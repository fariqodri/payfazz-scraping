const chunkTail = <T>(arr: T[], n: number, acc: T[][]): T[][] => {
    if (arr.length <= n) {
        acc.push(arr)
        return acc
    } else {
        acc.push(arr.slice(0,n))
        return chunkTail(arr.slice(n), n, acc)
    }
}

const chunk = <T>(arr: T[], n: number) => chunkTail(arr, n, [])

export default chunk