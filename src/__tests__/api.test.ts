import chai = require("chai")
import http = require('chai-http');
import { expect } from 'chai';
import {test} from "mocha"

import app from '../config/app';
import { IndexedDataModel, KursModel } from '../models';

chai.use(http);

const emptyIndexedDataTable = async () => {
    IndexedDataModel.deleteMany({}, () =>
        console.log('Indexed Data emptied')
    );
}

// const emptyKursDataTable = async () => {
//     KursModel.deleteMany({}, () => console.log('Kurs emptied'));
// }

const emptyTable = async () => {
    await emptyIndexedDataTable()
};

describe('Crawl API', () => {
    before(async () => {
        await emptyTable();
    });

    after(async () => {
        await emptyTable();
    });

    it('Should add data to database', async () => {
        return chai
            .request(app)
            .get('/api/indexing')
            .then(res => {
                expect(res.status).to.equal(200);
                expect(res.body).to.have.property("kurs")
                expect(res.body).to.have.property("head")
            });
    });
});

describe('Delete record by date', () => {
    const date = '2010-03-21';
    const fillDatabaseWithDate = async (date: Date) => {
        const indexedData = new IndexedDataModel({
            type: 'e-rate',
            datetime: new Date(date),
            kurs_data: [{
                currency: "USD",
                price: 15000,
                kurs_type: "beli"
            }]
        });
        indexedData.save()
    };

    before(async () => {
        await fillDatabaseWithDate(new Date(date))
    })

    test(`should delete all indexed data with date ${date}`, async () => {
        return chai
            .request(app)  
            .delete(`/api/kurs/${date}`)
            .then(res => {
                IndexedDataModel.countDocuments({datetime: new Date(date)}, (_, count) => expect(count).to.equal(0))
                expect(res.status).to.equal(200)
                expect(res.body).to.have.property("message")
            })
    })
});

describe("GET data by query", () => {
    const date = '2010-03-22';
    const tomorrow = '2010-03-23'
    const currency = "USD"
    const modelData = {
        type: 'e-rate',
        kurs_data: [{
            currency,
            price: 15000,
            kurs_type: "beli"
        }]
    }
    const fillTableWithDate = async () => {
        const datas = [{...modelData, datetime: new Date(date)}, {...modelData, datetime: new Date(tomorrow)}]
        IndexedDataModel.create(datas, (err) => {
            if (err) {
                console.error(err.message)
            }
        })
    }

    before(async () => {
        await emptyTable()
        await fillTableWithDate()
    })

    test(`it should return datas from ${date} to ${tomorrow}`, async () => {
        return chai
            .request(app)
            .get(`/api/kurs?startdate=${date}&enddate=${tomorrow}`)
            .then(res => {
                expect(res.status).to.equal(200)
                expect(res.body).to.have.property("data")
                expect(res.body.data).to.be.an("array")
            })
    })

    test(`it should return datas from ${date} to ${tomorrow} with currency ${currency}`, async () => {
        return chai
            .request(app)
            .get(`/api/kurs/${currency}?startdate=${date}&enddate=${tomorrow}`)
            .then(res => {
                expect(res.status).to.equal(200)
                expect(res.body).to.have.property("data")
                expect(res.body.data).to.be.an("array")
            })
    })
})

describe("ADD data", () => {
    before(async () => {
        await emptyIndexedDataTable()
    })
    const requestData = {
        "symbol": "AAA",
        "e_rate": {
            "jual": 1803.55,
            "beli": 177355
        },
        "tt_counter": {
            "jual": 1803.55,
            "beli": 177355
        },
        "bank_notes": {
            "jual": 1803.55,
            "beli": 177355
        },
        "date": "2018-05-16"
    }
    test("it should add data", async () => {
        return chai
            .request(app)
            .post("/api/kurs/")
            .send(requestData)
            .then(res => {
                expect(res.status).to.equal(200)
                expect(res.body.data).to.be.an("array")
                expect(res.body.data.length).to.equal(3)
            })
    })

    test("it should update data", async () => {
        const {symbol, e_rate, date} = requestData
        const updateData = {symbol, e_rate, date}
        return chai
            .request(app)
            .put("/api/kurs/")
            .send(updateData)
            .then(res => {
                expect(res.status).to.equal(200)
                expect(res.body.data).to.be.an("array")
                expect(res.body.data.length).to.equal(1)
            })
    })
})
